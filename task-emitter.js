const { fork } = require('child_process');
const child_eventBasedTaskEmitter = fork('./controllers/contr-task-eventBased');
const child_scheduledTaskEmitter = fork('./controllers/contr-task-scheduled');

var logger = require('./_dev/nt-logger');
logger.log("Task Emitter gestartet...");

child_eventBasedTaskEmitter.send('start');
child_scheduledTaskEmitter.send('start');


