var mongodb = require('mongodb');
var ObjectId = mongodb.ObjectID;

const schedule = require('node-schedule');
const async = require('async');
const _ = require('lodash');

const mdl_mongogen = require('../models/mdl-mongodb-generics');
const mdlEventBased = require('../models/mdl-eventBased');
const contrInflux = require('./contr-influx');
const moment = require('moment');
const senecaClient = require('./contr-seneca')

var rule = new schedule.RecurrenceRule();
rule.second = 8;
const DELAY = 15 * 1000;



var notifyUserinTaskDocument = (err, doc, result ,callback) => {
    if(err) return callback()
    if(_.isEmpty(doc.Personal)) return callback();
    doc.Personal.forEach((pers, i)=>{ 
        var data = {};
        data.notification = {title:"Neue Aufgabe", body: doc.Bezeichnung };
        data._id = pers._id;       
        data.taskid = result.insertedId;
        senecaClient.act('pushnotify:user', { data })
    })

    callback();
    
}

var createNewTaskDocument = function (document, callback) {
    // console.log("...Dokument" + document._id + "hat die Bedingung erfüllt")
    var q = mdlEventBased.triggerdTaskSetDefinition(document);
    mdl_mongogen.addNew("Tasks", q.set , callback)
}

var hasTargetValueReachedCondition = function (document, callback) {
    // console.log("...Prüfe Dokument", document._id)
    
    var tv = parseFloat(document.event["target-value"]);
    var condition = document.event.condition
    condition = /gt|lt|eq|gte|lte/.test(condition) ? condition : "gte";    
    var dbquery = contrInflux.queryBuilder(document.event.databasecfg);
    if (!dbquery) return callback(error);

    var db = document.influxDB;
    contrInflux.reqInflux(db, dbquery, (err, result) => {
        if (!err && !_.isEmpty(result)) {            
            //  console.log("...Zielwert:", tv)
            //  console.log("...gelesener Wert:",  _.last(result).value)
            //  console.log("...Bedingung:" , condition, _[condition](tv, _.last(result).value))
            callback(_[condition](tv, _.last(result).value))
        }
        else {
            callback(null);
        }
    });
}

var hasDocumentTriggerBefore = function (document) {
    return !_.isEmpty(_.filter(document.triggeredTasks, o => ( o.state !== "done"))) 
}

var hasAllDocumentFinished = function (document) {
    var fine = _.filter(document.triggeredTasks, 'isFinished');    
    return fine.length === document.triggeredTasks.length
}

var walkThroughSets = (results, callback) => {    
    async.each(results, (doc, cb) => {
        // console.log("...Bearbeite Dokument", doc._id)
        if ((!hasDocumentTriggerBefore(doc) || hasAllDocumentFinished(doc))&& _.has(doc,["event","databasecfg"])) {
            hasTargetValueReachedCondition(doc,
                (val) => { val ? createNewTaskDocument(doc, (err,result)=> notifyUserinTaskDocument(err , doc, result, cb)) : cb() })
        } 
        else {            
            cb()
        }

    },(err) => {         
        callback(err, null) }
    )
}

var getAllEventBasedTaskfromDatabase = (callback) => {
    var stages = mdlEventBased.stageDefinition();
    mdl_mongogen.aggregate("Tasks", stages, callback)
}

var initTaskEmitter = () => {
    // console.log(moment().format("YYYY-MM-DD HH:mm:ss"), " ...Eventtask-Emitter gestartet" )
    getAllEventBasedTaskfromDatabase((err, results) => {
        if (!err) {
            walkThroughSets(results, () => {
                //console.log(moment().format("YYYY-MM-DD HH:mm:ss"), " ...Eventtask-Emitter beendet" )
            });
        } 
        else {
            console.log(err);
        }
    });
}

var eventBasedTaskEmitter = function () {    
    setInterval(initTaskEmitter, DELAY)   
}

process.on('message', msg => msg === "start" && eventBasedTaskEmitter())

module.exports = eventBasedTaskEmitter;