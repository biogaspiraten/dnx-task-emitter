const client = require('seneca')()
  .use('seneca-amqp-transport')
  .client({
    type: 'amqp',
    pin: ['pushnotify:*'],
    url: 'amqp://northtec:humpiedumpie@10.10.100.25:5672'
  });



module.exports = client;