﻿﻿var urlConfig = {
    //'thartmann' : 'mongodb://192.168.222.25:27017/Dev_Dingx_Database',
    'thartmann' : 'mongodb://192.168.66.179:27017/Dev_Dingx_Database', //Virtual Machine
    'ygoldgräbe': 'mongodb://localhost:27017/DingX_2206', //Developer Machine to NTOnline Server   
    'pdreyer': 'mongodb://192.168.222.25:27017/Dev_Dingx_Database',
    'chrender': 'mongodb://127.0.0.1:27017/NTonline_BigDataStore',
    'production': 'mongodb://website_access_user:lN3eeG5b5qktXlHW@10.10.100.25:27017/NTonline_BigDataStore?authMechanism=SCRAM-SHA-1&authSource=NTonline_BigDataStore' //Production Machine
}

var dbNames = {
    'thartmann' : 'Dev_Dingx_Database',
    'ygoldgräbe' : 'DingX_2206',
    'pdreyer' : 'Dev_Dingx_Database',
    'chrender' : 'NTonline_BigDataStore',
    'production': 'NTonline_BigDataStore',
}

var getURLbyEnviroment = () => {    
    let username = process.env.USERNAME ? process.env.USERNAME : process.env.USER;
    if (username && urlConfig[ username ]) {
        console.log("MongoDB Connection by Username: " + username);
        return  { url: urlConfig[username], dbName: dbNames[username] }
    } else {
        console.log("MongoDB Connection by Enviroment: " + (process.env.NODE_ENV || 'production') )
        return { url: urlConfig[process.env.NODE_ENV ] || urlConfig.production, dbName: dbNames['production'] }
    }
}



module.exports = getURLbyEnviroment();