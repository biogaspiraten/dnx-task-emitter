const mongodb = require('mongodb');
const mdl_mongogen = require('../models/mdl-mongodb-generics');
const mdl_scheduled = require('../models/mdl-scheduled');
const ObjectId = mongodb.ObjectID;
var util = require('util');
var _ = require('lodash');
const async = require('async');

var RRule = require('rrule').RRule;
var Rrecur = require('rrecur').Rrecur;

var schedule = require('node-schedule');

var moment = require('moment');
var intervallId = null;

var queryAsArray = [
    {
        $match: {
            "isScheduled": true,
            "rruleData": { $gt: {}}
        }
    }/*,
    {
        $project: {
            //"rruleData": 1
        }
    }*/
];

//var repeatTime = 6000;
var repeatTime = 600000; //alle 10 Minuten
var scheduleCounter = 0;

//Nur für Tägliche recurrences. Wenn stündliche oder minütliche wiederholungen auch funktionieren sollen, dann das Date für den Schedulejob anpassen.
var initScheduledTaskEmitter = function () {
    console.log(moment().format("YYYY-MM-DD HH:mm:ss"), '- Next repetition in ' + repeatTime/1000/60 + ' minutes.');
    clearAllScheduledTasks();
    var taskCounter = 0;
    scheduleCounter = 0;
    var freshTasks = [];
    var tasks = [];

    //alle scheduled Tasks raussuchen.
    mdl_mongogen.aggregate("Tasks", queryAsArray,
        function (err, result) {
            console.log(moment().format("YYYY-MM-DD HH:mm:ss"), '- Got all tasks to be scheduled.');
            //console.log(result);
            console.log('Found ' + result.length + ' Tasks to be scheduled.');
            result.forEach(function (entry) {
                if (_.isEmpty(entry.rruleData))  return                                
                var rruleData = entry.rruleData;

                //Um die Einträge zu begrenzen, damit Sie nicht ins unendliche gehen und den Server völlig überlasten bei weiterer Verarbeitung.
                if (!rruleData.until) {
                    rruleData.until = "2019-01-01T00:00:00.000Z";
                }

                //rruleData Array in eine recurrence rule nach https://tools.ietf.org/html/rfc5545 umwandeln und diese rule als RRule Object
                var rfcString = Rrecur.stringify(rruleData);
                var rule = RRule.fromString(rfcString);

                //das RRule Object kann alle Daten als Array zurückgeben.
                var dateArray = rule.all();

                //console.log(entry._id + ' Scheduled Dates: ', dateArray);
                //var testArray = ["2017-08-03T11:07:00.000Z", "2017-08-03T11:07:15.000Z", "2017-08-03T11:07:30.000Z", "2017-08-03T11:07:45.000Z"];
                //var testArray2 = ["2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z"];
                //var testArray2 = ["2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z", "2017-08-03T12:03:00.000Z"];

                //eine Kopie des Tasks erstellen und anpassen für die Neueinfügung.
                //mdl_mongogen.findOne("Tasks", ObjectId(entry._id), {_id: 0}, function (err, data) {
                    taskCounter++;
                    var freshTask = _.clone(entry);
                    freshTask._parent_id = ObjectId(entry._id);
                    freshTask["task-type"] = "repeated";
                    freshTask["state"] = "open";
                    freshTask.isScheduled = false;
                    freshTask.isEventBase = false;
                    freshTask.event = null;
                    freshTask.rruleData = {};

                    delete freshTask._id;
                    freshTask.Personal.forEach(function (key) {
                        delete key.schedulerData;
                    });

                    var timeBefore = freshTask.scheduleTrigger;
                    var scheduleDate = freshTask['Soll-Beginn'];
                    var timeOnly = moment(scheduleDate).format("HH:mm:ss");

                    var duration = moment.duration(timeBefore);

                    console.log('['+ taskCounter + '/' + result.length +'] - ', moment().format("YYYY-MM-DD HH:mm:ss") + ' - Created fresh copy of task ID', entry._id);

                    var date1  = new Date(freshTask['Soll-Beginn']);
                    var date2 = new Date(freshTask['Soll-Ende']);

                    //Abstand zwischen Soll-Beginn und Soll-Ende für späteres hinzufügen zum neuem Datum
                    var difference = moment(date2).diff(moment(date1));
                    var durationDifference = moment.duration(difference);

                    freshTasks.push(freshTask);

                    //für jedes Datum wird ein scheduleJob zu der entsprechenden Auslösezeit erstellt.
                    editTask(freshTasks[taskCounter - 1], duration, timeOnly, durationDifference, dateArray, tasks);
                    console.log('['+ taskCounter + '/' + result.length +'] -', moment().format("YYYY-MM-DD HH:mm:ss") + ' - Set scheduleJob for task ID', entry._id);
                    if(taskCounter === result.length){
                        console.log('Waiting for next repetition...');
                    }
                //});
            });
        }
    );
};

var editTask = function(freshTask, duration, timeOnly, durationDifference, dateArray, tasks){
    dateArray.forEach(function (entry) {
        scheduleCounter++;
        var editedTask = _.clone(freshTask);
        //console.log(editedTask);

        //erst nur das Datum aus der recurrence extrahieren und dann mit der Soll-Beginn Zeit befüllen.
        var dateOnly = moment(entry).format("YYYY-MM-DD");
        var dateTime = dateOnly + "T" + timeOnly + "+02:00";

        //anschließend die scheduleTrigger Duration abziehen, damit der Task vorzeitig erstellt wird.
        var dateSubtracted = moment(dateTime).subtract(duration);
        var date = new Date(dateSubtracted);

        //dem Task einen einzigartigen Namen geben - ist wichtig für clearAllScheduledTasks()
        var jobName = 'scheduledTask' + scheduleCounter;

        //Soll-Beginn und Soll-Ende Datum anpassen.
        //Abstand zwischen Soll-Beginn und Soll-Ende dem neuem Date hinzufügen.
        editedTask['Soll-Beginn'] = new Date(dateTime);
        var newSollEnde = moment(dateTime).add(durationDifference);
        editedTask['Soll-Ende'] = new Date(newSollEnde);

        tasks.push(editedTask);
        scheduleTask(jobName, date, tasks[scheduleCounter - 1]);
    });
};

var scheduleTask = function (jobName, date, doc){
    //console.log('scheduleTask set to ' + date);
    schedule.scheduleJob(jobName, date, function () {
        async.waterfall([
            (cb) => incrementSequenceID(doc, () => cb(null,doc)),               
            (doc, cb) => setRank(doc, () => cb(null, doc)),
            (doc, cb) => setStateOpen(doc, cb),
            (doc, cb) => mdl_mongogen.addNew("Tasks", doc, cb)

        ], (err, result) => {
                if (err) {
                    throw err;
                } else {
                    console.log(moment().format("YYYY-MM-DD HH:mm:ss"), '- Task hinzugefügt.');
                }
            });
    });
};

var clearAllScheduledTasks = function () {
    console.log('Clearing previous scheduled tasks...');
    var jobList = schedule.scheduledJobs;
    var taskAmount = Object.keys(jobList).length;

    for (var i = 0; i < taskAmount; i++) {
        var taskID = i + 1;

        if (jobList['scheduledTask' + taskID] !== undefined) {
            jobList['scheduledTask' + taskID].cancel();
        }
    }
};

var scheduledTaskEmitter = function () {
    console.log("Run scheduled task - Emitter");

    initScheduledTaskEmitter();
    setInterval(initScheduledTaskEmitter, repeatTime);
};

function incrementSequenceID(document, callback){
    var shortName = "";
    if(!_.isEmpty(document.Projekt)){
        shortName = document.Projekt.Bezeichnung;
        shortName = shortName.replace(/[aeiou]/ig,'').toUpperCase().slice(0, 4);
    } else {
        shortName = "NOP";
    }

    mdl_mongogen.findOne("Counters", {projectKey: shortName, "Kunde._id": document.Kunde._id}, {}, function(err, doc){
        if(!doc){
            var doc = {
                "projectKey": shortName,
                "colRef": "Tasks",
                "sequence_value": 1,
                "Kunde": {
                    _id: document.Kunde._id,
                    Kundenname: document.Kunde.Bezeichnung
                }
            };

            mdl_mongogen.addNew("Counters", doc, function(err, result){});
            document.customID = shortName + "-1";            
            callback(null, document);
        } else {
            getNextSequenceValue(document.Kunde._id, shortName, function(nextSequenceValue){
                document.customID = shortName + "-" + nextSequenceValue;                
                callback(null, document);
            });
        }
    });
}

function getNextSequenceValue(customerID, sequenceName, callback){
    mdl_mongogen.findAndModify("Counters", {projectKey: sequenceName, "Kunde._id": customerID}, {$inc: {sequence_value: 1}}, {upsert: true}, function(err, result){
        var sequenceDocument = result.value;
        callback(sequenceDocument.sequence_value);
    });
 }

 function setRank(doc, callback) {
    var q = mdl_scheduled.findHighestRankQueryDefinition(doc.Kunde._id);
    mdl_mongogen.findAllSortNLimit("Tasks", q.query, q.proj, q.sort, q.limit, (err,result) => {
        if(err || !_.isEmpty(result)) {
            doc.rank = result[0].rank + 1;
            doc.afterCustomID = result[0].customID;
        }
        callback(null, doc);                 
    })
}

function setStateOpen(doc, callback){
    mdl_mongogen.update("Tasks", {_id: doc._parent_id}, {state: "open"}, function(err, result){
        if(!err){
            console.log('Changed state of parent Task to "open"');
        }
        callback(null, doc);
    });
}

process.on('message', msg => msg === "start" && scheduledTaskEmitter())

module.exports = scheduledTaskEmitter;