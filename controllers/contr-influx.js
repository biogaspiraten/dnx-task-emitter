const InfluxDB = require('influx').InfluxDB;
const _ = require('lodash')

var influxConfig = {
    host: '10.10.100.25',
    port: 8086, // optional, default 8086
    protocol: 'http', // optional, default 'http'
    username: 'northtec',
    password: 'humpiedumpie',
    database: null
    
}

const client = function (database) {
    return new InfluxDB(_.extend(influxConfig, { database }))
}

var influxDefinition = {


    queryBuilder: function(dbconfig){
        
        var timeRangeVal = dbconfig.timeRangeVal || "0";
        var measurement = dbconfig.measurement;
        var topic = dbconfig.topic;
        var fieldkey = dbconfig.fieldkey
        var correctionfactor = dbconfig.correctionfactor || dbconfig.Korrekturfaktor
        var dbquery = influxQueryBuilder(timeRangeVal, measurement, topic, fieldkey, correctionfactor);
        return dbquery
    },

    reqInflux: function (db, dbquery,callback) {      
        
        client(db).query(dbquery.query)
        .then(results => {            
            if (results) {
                var resultvaluedefinition = _
                    .chain(results)
                    .flattenDeep() 
                    .map(o => {
                        return {
                            time: o.time,
                            value: o[dbquery.key]
                        }
                    })
                    .value();         

                callback(null, resultvaluedefinition);
            }
            else {
                callback(true, null)
            }
        }) 
        .catch(err => {            
            console.log("reqInflux",err)
            callback(err, null);
        }) 
    }

}

var error = {
    succes: false,
    message: "Keine Daten vorhanden"
}

function influxQueryBuilder(timeRangeVal, measurement, topic, fieldkey, correctionfactor) {
    if(!(timeRangeVal && measurement && topic) || (/bitte/i.test(measurement))) return null    
        correctionfactor = correctionfactor ? correctionfactor : "1"
        var timeintervall = parseInt(timeRangeVal);
        var fk = fieldkey || "value"
        switch (timeintervall) {
            case 0:
                return {
                    query: "SELECT last(" + fk + ") * " + correctionfactor + " FROM "
                    + measurement + " WHERE topic=\'"
                    + topic + "\'",
                    key : "last"
                }
            case 1:
                return {
                    query: "SELECT mean(" + fk + ")  FROM "
                    + measurement + " WHERE topic=\'"
                    + topic + "\' AND time > now() - 1h" + " GROUP BY time(1h)",
                    key: "mean"
                }
            case 2:
                return {
                    query: "SELECT mean(" + fk + ")  FROM "
                    + measurement + " WHERE topic=\'"
                    + topic + "\' AND time > now() - 12h" + " GROUP BY time(12h)",
                    key: "mean"
                }
            case 3:
                return {
                    query: "SELECT mean(" + fk + ")  FROM "
                    + measurement + " WHERE topic=\'"
                    + topic + "\' AND time > now() - 24h" + " GROUP BY time(24h)",
                    key: "mean"
                }

            case 4:
                return {
                    query: "SELECT mean(" + fk + ")  FROM "
                    + measurement + " WHERE topic=\'"
                    + topic + "\' AND time > now() - 30d" + " GROUP BY time(30d)",
                    key: "mean"
                }
            case 5:
                return {
                    query: "SELECT mean(" + fk + ")  FROM "
                    + measurement + " WHERE topic=\'"
                    + topic + "\' AND time > now() - 12w" + " GROUP BY time(12w)",
                    key: mean
                }
            default:
                return {
                    query: "",
                    key: ""
                }
        }   
    
}


module.exports = influxDefinition;