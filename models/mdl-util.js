﻿var mongodb = require('mongodb');
var ObjectId = mongodb.ObjectID;
var _ = require('lodash')

function listRegextQuerysByKeywords(keywords, value) {
    var arr = []
    for (var index = 0; index < keywords.length; index++) {
        var obj = {};
        obj[keywords[index]] = {
            $regex: `.*\\Q${value}\\E.*`,
            $options: 'i'
        }
        arr.push(obj);
    }
    return arr
}


var model_Utilitys = {
    CustomerExtractor: function (req) {
        var kunde = req.session.passport.user._Kunde;
        kunde._id = ObjectId(kunde._id);
        return kunde;
    },

    CustomerCreator: function (req) {
        var kunde = {}
        kunde._id = ObjectId(req.session.passport.user._Kunde._id);
        kunde.Bezeichnung = req.session.passport.user._Kunde.Kundenname
        return kunde;
    },

    ProjectExtractor: function (req) {
        var project = _.get(req.session.passport.user, ["Projekt"]) || {};
        project._id = ObjectId(project._id);
        return project;
    },

    projectEnhancer: function (req, data) {
        var projectId = _.get(req.session.passport.user, ["Projekt", "_id"]);
        if (projectId) {
            var project = _.pick(req.session.passport.user, ["Projekt"])
            project.Projekt._id = ObjectId(projectId);
            return data ? _.extend(project, _.omit(data, ["Projekt"])) : project;
        } else {
            return _.omit(data, ["Projekt"])
        }

    },

    decoradeQueryByState: function (req, essentialqueryobject) {
        var customerId = ObjectId(_.get(req.session.passport.user, ["_Kunde", "_id"]));
        var customer = {
            "Kunde._id": customerId
        }

        var projectId = _.get(req.session.passport.user, ["Projekt", "_id"]);
        var project = projectId ? {
            "Projekt._id": ObjectId(projectId)
        } : {}
        return {
            withCustomer: function (essentialqueryobject) {
                return _.extend(essentialqueryobject, customer)
            },
            withProject: function (essentialqueryobject) {
                return _.extend(essentialqueryobject, customer, project)
            },
            addSearch: function (essentialqueryobject, keywords, value) {
                if (!value || _.isEmpty(value)) {
                    return essentialqueryobject;
                } else {
                    return _.extend(essentialqueryobject, {
                        "$or": listRegextQuerysByKeywords(keywords, value)
                    })
                }
            }
        }

    },

    aggregationPagination: function (skip, limit) {
        var arr = [];
        skip ? arr.push({
            $skip: parseInt(skip)
        }) : null;
        limit ? arr.push({
            $limit: parseInt(limit)
        }) : null;
        return arr;
    },

    replaceKey: function (key, origin) {
        if (origin.hasOwnProperty(key)) {
            return origin[key]
        } else {
            return key
        }
    },

    aggregationSkipNLimit: function (query) {
        var arr = [];
        if (query.skip) {
            arr.push({
                $skip: parseInt(query.skip)
            })
        }
        if (query.limit) {
            arr.push({
                $limit: parseInt(query.limit)
            })
        }
        return arr;
    }

}

module.exports = model_Utilitys;