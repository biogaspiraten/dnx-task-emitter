var mongodb = require('mongodb');
var MongoClient = require('mongodb').MongoClient;
var { url, dbName } = require('../controllers/mongodb-connection');

var dbConnection = (callback) => {
    MongoClient.connect(url, function (err, client) {      
        if (err) return console.log(err)      
        var db = client.db(dbName);
        callback(db, client)
    })
};

module.exports = dbConnection;