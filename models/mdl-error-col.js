﻿var errors = {
    gendb: "Database error"
}

var errCol = function (errname) {
    return {
        success: false,
        message: errors[errname] ? errors[errname] : "Fehlerbezeichnung nicht gefunden"
    }
}

module.exports = errCol;