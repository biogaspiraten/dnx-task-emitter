﻿var mongodb = require('mongodb');
var ObjectId = mongodb.ObjectID;
var assert = require('assert');
var async = require('async');
var _ = require('lodash');
var url = require('../controllers/mongodb-connection');
var dbConnection = require('./mdl-mongodb-connect');
var errorcollection = require('./mdl-error-col')

var model = {
    // das Callback ist ein Cursor
    findPagedData: function (Collection, query, projection, skp, lim, callback) {
        dbConnection((db, client) => {
            var cursor = db.collection(Collection).find(query, { projection }).skip(parseInt(skp)).limit(parseInt(lim));
            cursorCall(client, cursor, callback);
        });
    },

    findOne: function (Collection, query, projection, callback) {
        dbConnection((db, client) => {
            db.collection(Collection).findOne(query, projection, (err, doc) => {
                client.close();
                callback(err, doc);
            });
        });
    },

    findAll: function (Collection, query, prjctn, callback) {
        dbConnection((db, client) => {
            db.collection(Collection).find(query).project(prjctn).toArray()
                .then((docs) => {
                    client.close();
                    callback(null, docs)
                }, (err) => {
                    client.close();
                    callback(err, null)
                });
        });
    },

    findAllSorted: function (Collection, query, prjctn,sort, callback) {
        dbConnection((db, client) => {
            db.collection(Collection)
                .find(query)
                .project(prjctn)
                .sort(sort)
                .toArray()
                .then((docs) => {
                    client.close();
                    callback(null, docs)
                }, (err) => {
                    client.close();
                    callback(err, null)
                });
        });
    },

    findAllSortNLimit: function (Collection, query, prjctn,sort,limit,callback) {
        dbConnection((db, client) => {
            db.collection(Collection)
                .find(query)
                .project(prjctn)
                .sort(sort)
                .limit(limit)
                .toArray()
                .then((docs) => {
                    client.close();
                    callback(null, docs)
                }, (err) => {
                    client.close();
                    callback(err, null)
                });
        });
    },

    deleteOnebyId: function (Collection, id, callback) {
        dbConnection((db, client) => {
            db.collection(Collection).deleteOne({ _id: ObjectId(id) }, function (err, result) {
                client.close();
                callback(err, result);
            });
        });
    },

    deleteOnebyQuery: function (Collection, query, callback) {
        dbConnection((db, client) => {
            db.collection(Collection).deleteOne(query, function (err, result) {
                client.close();
                callback(err, result);
            });
        });
    },

    deleteMany: function (Collection, query, callback) {
        dbConnection((db, client) => {
            db.collection(Collection).deleteMany(query, null, (err, result) => {
                client.close();
                callback(err, result);
            });
        });
    },

    addNew: function (Collection, data, callback) {
        dbConnection((db, client) => {
            db.collection(Collection).insertOne(data, function (err, result) {
                client.close();
                callback(err, result);
            });
        });
    },

    addMany: function (Collection, data, callback) {
        dbConnection((db, client) => {
            db.collection(Collection).insertMany(data, function (err, result) {
                client.close();
                callback(err, result);
            });
        });
    },

    updateOrInsert: function (Collection, query, data, callback) {
        dbConnection((db, client) => {
            db.collection(Collection).updateOne(query, { $set: data }, { upsert: true }, function (err, result) {
                client.close();
                callback(err, result);
            });
        });
    },

    update: function (Collection, query, data, callback) {
        dbConnection((db, client) => {
            db.collection(Collection).updateOne(query, { $set: data }, function (err, result) {
                client.close();
                callback(err, result);
            });
        });
    },

    updateMany: function (Collection, query, data, callback) {
        dbConnection((db, client) => {
            db.collection(Collection).updateMany(query, { $set: data }, function (err, result) {
                client.close();
                callback(err, result);
            });
        });
    },

    updateUnset: function (Collection, query, data, callback) {
        dbConnection((db, client) => {
            db.collection(Collection).updateOne(query, { $unset: data }, function (err, result) {
                client.close();
                callback(err, result);
            });
        });
    },

    updateManyUnset: function (Collection, query, data, callback) {
        dbConnection((db, client) => {
            db.collection(Collection).updateMany(query, { $unset: data }, function (err, result) {
                client.close();
                callback(err, result);
            });
        });
    },

    updateAddToSet: function (Collection, query, data, callback) {
        dbConnection((db, client) => {
            db.collection(Collection).updateOne(query, { $addToSet: data }, function (err, result) {
                client.close();
                callback(err, result);
            });
        });
    },

    updateAddToArray: function (Collection, query, data, callback) {
        dbConnection((db, client) => {
            db.collection(Collection).updateOne(query, { $push: data }, function (err, result) {
                client.close();
                callback(err, result);
            });
        });
    },

    updateRemoveFromSet: function (Collection, query, data, callback) {
        dbConnection((db, client) => {
            db.collection(Collection).updateOne(query, { $pull: data }, function (err, result) {
                client.close();
                callback(err, result);
            });
        });
    },

    updateRemoveFromSet: function (Collection, query, data, callback) {
        dbConnection((db, client) => {
            db.collection(Collection).updateOne(query, { $pull: data }, function (err, result) {
                client.close();
                callback(err, result);
            });
        });
    },

    deleteFromArray: function (Collection, query, data, callback) {
        dbConnection((db, client) => {
            db.collection(Collection).updateOne(query, { $pull: data }, function (err, result) {
                client.close();
                callback(err, result);
            });
        });
    },

    aggregate: function (Collection, queryasArray, callback) {
        dbConnection((db, client) => {
            db.collection(Collection).aggregate(queryasArray).toArray(function (err, result) {
                client.close();
                callback(err, result);
            });
        });
    },

    aggregateUpgraded: function (Collection, queryasArray, callback) {
        dbConnection((db, client) => {
            db.collection(Collection).aggregate(queryasArray, { allowDiskUse: true }).toArray(function (err, result) {
                client.close();
                callback(err, result);
            });
        });
    },

    aggregateWithCount: function (Collection, queryasArray, callback) {
        dbConnection((db, client) => {
            async.parallel([
                (cb) => { db.collection(Collection).aggregate(queryasArray).toArray(cb) },
                (cb) => {
                    var filteredquery = queryasArray.filter((o) => { return !(o.hasOwnProperty("$limit") || o.hasOwnProperty("$skip")) })
                    filteredquery.push({ "$count": "count" });
                    db.collection(Collection).aggregate(filteredquery).toArray(cb)
                }
            ], (err, result) => {
                client.close();
                if (!_.isEmpty(result[0])) {
                    callback(err, { store: result[0], count: _.get(result,["1","0","count"]) });
                }
                else {

                    callback(err, { store: [], count: 0 })
                }
            })
        });
    },

    bulkUpdateWrite: function (Collection, bulksetAsArray, callback) {
        dbConnection((db, client) => {
            db.collection(Collection).bulkWrite(bulksetAsArray, { ordered: true }, function (err, result) {
                client.close();               
                callback(err, result);
            });
        });
    },

    distinct: function (Collection, key, query, callback) {
        dbConnection((db, client) => {
            db.collection(Collection).distinct(key, query, function (err, result) {
                client.close();
                callback(err, result);
            });
        });
    },

    count: function (Collection, query, callback) {
        dbConnection((db, client) => {
            db.collection(Collection).count(query, function (err, result) {
                client.close();
                callback(err, result);
            });
        });
    },

    execute: function(command, collection, querycontainer, callback) {
        var data = querycontainer.proj || querycontainer.set        
        dbConnection((db, client) => {
            db.collection(collection)[command](querycontainer.query, data, function (err, result) {                      
                client.close();
                if (callback.length > 1) {
                    callback(err, result);
                } else {
                    !err ? callback(result) : callback(this.getError("gendb"))
                }
            });
        });        
    },

    findAndModify: function (Collection, query, doc, options, callback) {
        dbConnection((db, client) => {
            db.collection(Collection).findOneAndUpdate(query,  doc, options, function (err, result) {
                client.close();
                callback(err, result);
            });
        });
    },

    getError: errorcollection
}

var cursorCall = function (client, cursor, callback) {
    if (cursor != null) {
        callback(client, cursor);
    }
    else {
        client.close();
        callback(null, null);
    }
}

module.exports = model;