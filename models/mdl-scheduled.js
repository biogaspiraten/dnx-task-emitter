const mongodb = require('mongodb')
const ObjectId = mongodb.ObjectID
const _ = require('lodash')

var mdlScheduled = {
    findHighestRankQueryDefinition: (customerID) => {
        return {
            query: {
                "Kunde._id": customerID,
            },
            proj: {
                rank:1, 
                customID:1, 
                afterCustomID:1
            },
            sort: {
                rank:-1
            },
            limit:1
        }
    }
}

module.exports = mdlScheduled;