const mongodb = require('mongodb')
const ObjectId = mongodb.ObjectID
const _ = require('lodash')

var mdlEventBased = {
    queryDefinition: function () {
        return {
            query: {
                "event.isEventBased": true,
                "task-type": "single",
                "$or": [
                        { "isFinished": { "$exists": false }},
                        { "isFinished": false }
                ] 
                    
            },
            proj: {}
        }
    },
    stageDefinition: function () {
        var stages = []


        stages = [

            {
                $match: {
                    "isEventBased": true,                   
                }
            },
            {
                $lookup: {
                    "from": "Customers",
                    "localField": "Kunde._id",
                    "foreignField": "_id",
                    "as": "customer"
                }
            },
            {
                $lookup: {
                    "from": "Tasks",
                    "localField": "_id",
                    "foreignField": "_parent_id",
                    "as": "triggeredTasks"
                }
            },

            {
                $project: {
                    "_id": 1,
                    "Kunde": 1,
                    "Projekt": 1,
                    "Ort":1,
                    "Bezeichnung": 1,
                    "Beschreibung": 1,
                    "Personal": 1,
                    "Bezug": 1,
                    "_parent_id": 1,
                    "task-type": 1,
                    "links": 1,
                    "isInternal": 1,
                    "event": 1,
                    "Benutzereingaben": 1,
                    "Benachrichtigung": 1,
                    "triggeredTasks": 1,
                    "Soll-Zeitaufwand": 1,
                    "Ist-Zeitaufwand": 1,
                    "Soll-Beginn" : 1, 
                    "Soll-Ende" : 1,
                    "Ist-Beginn" : 1,
                    "Ist-Ende" : 1,
                    "influxDB": { "$arrayElemAt": ["$customer._config.database-access.influxDB", 0] }

                }
            },

        ]
        return stages;
    },
    triggerdTaskSetDefinition: function ( document ) {
        document["task-type"] = "triggered";
        document["isScheduled"] = false;
        document["isEventBased"] = false;
        document["event"] = null;
        document["_parent_id"] = ObjectId(document._id);
        document["Soll-Beginn"] = new Date();
        return { set: _.omit(document, ["_id","triggeredTasks","influxDB","rruleData"]) }
    }

}

module.exports = mdlEventBased;