﻿const winston = require('winston');


var logger = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)({
            colorize: true,
            prettyPrint: true,
            depth: 3

        }),       
        new (winston.transports.File)({
            name: 'error-file',
            filename: 'error.log',
            level: "error",
            handleExceptions: false,
            humanReadableUnhandledException: false,
            maxsize: 100000,
            maxFiles: 10
        })
     
        
    ],
    exitOnError: true,

});

module.exports = logger;